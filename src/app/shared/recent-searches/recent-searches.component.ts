import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { MusicSearchService } from 'src/app/core/services/music-search/music-search.service';

@Component({
  selector: 'app-recent-searches',
  templateUrl: './recent-searches.component.html',
  styleUrls: ['./recent-searches.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RecentSearchesComponent implements OnInit {

  queries: string[] = []

  constructor(
    private cdr: ChangeDetectorRef,
    private service: MusicSearchService
  ) { }

  ngOnInit(): void {
    this.service.queryChanges.subscribe({
      next: query => {
        this.queries.unshift(query)
        this.queries = this.queries.slice(0, 5)
        this.cdr.markForCheck()
      }
    })
  }

  search(query: string){
    this.service.searchAlbums(query)
  }

}
