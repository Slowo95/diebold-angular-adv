
export interface Playlist {
  id: string;
  name: string;
  /**
   * IS playlist public?
   */
  public: boolean;
  description: string;
}
