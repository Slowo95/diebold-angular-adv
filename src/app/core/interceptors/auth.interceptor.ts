import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import { concat, EMPTY, from, iif, Observable, of, pipe, range, throwError, timer, zip } from 'rxjs';
import { AuthService } from '../auth/auth/auth.service';

import { catchError, delay, filter, map, mergeAll, mergeMap, pluck, retry, retryWhen, tap } from "rxjs/operators";

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private auth: AuthService) { }

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler): Observable<HttpEvent<unknown>> {

    const exponentialBackoff = <T>(retries = 3) => pipe(
      retryWhen<T>(errors => { // emit next to retry, complete to giveup, error to error
        const connectionErrors = errors.pipe(
          mergeMap(error => iif(
            () => error instanceof HttpErrorResponse && error.status === 0,
            of(error),
            throwError(error)))
        )
        // zip - import 'static' constrictor (not operator) from rxjs (not /operators)
        return zip(range(1, retries), connectionErrors).pipe(
          mergeMap(([i, error]) => {
            return iif(
              () => i < retries,
              timer(i * i * 500),
              throwError(error)
            )
          }),
        )
      }),
    )

    //Request is IMMUTABLE:
    const authrequest = this.authorizeRequest(request)

    return next.handle(authrequest).pipe(
      exponentialBackoff(),
      catchError((error, caughtSource) => {
        if (!(error instanceof HttpErrorResponse)) {
          console.error(error)
          return throwError(new Error('Unexpected Error!'))
        }
        if (error.status === 0) {
          return throwError(new Error('Connection timeout after 3 retries'))
        }
        if (error.status === 401) {
          return from(this.auth.login()).pipe(
            mergeMap(() => {
              if (!this.auth.getToken()) { return throwError(error) }
              return next.handle(this.authorizeRequest(request))
            }),
          )
        }
        return throwError(new Error(error.error.error.message))
      })
    )
  }

  private authorizeRequest(request: HttpRequest<unknown>) {
    return request.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.auth.getToken()
      }
    });
  }
}

// Chain of responsibility
// HttpClient.next = A 
// A.next = B 
// B.next = C 
// C.next = HttpBackend 

// obs = HttpClient.request(R)