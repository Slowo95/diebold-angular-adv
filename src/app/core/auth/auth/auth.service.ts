import { Injectable } from '@angular/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private oauth: OAuthService) {
    this.oauth.configure(environment.authLoginFlowConfig)
    this.oauth.setupAutomaticSilentRefresh()
  }

  getToken() {
    return this.oauth.getAccessToken()
  }

  async init() {
    await this.oauth.tryLoginImplicitFlow()

    console.log(this.oauth.getAccessToken())
    if (!this.oauth.getAccessToken()) {
      this.oauth.initLoginFlow()
    }
  }

  async login() {
    // this.oauth.initLoginFlow()
    await this.oauth.initLoginFlowInPopup()
  }

  async logout() { }
}
