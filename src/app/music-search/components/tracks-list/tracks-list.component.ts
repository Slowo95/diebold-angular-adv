import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Track } from 'src/app/core/model/search';

@Component({
  selector: 'app-tracks-list',
  templateUrl: './tracks-list.component.html',
  styleUrls: ['./tracks-list.component.scss']
})
export class TracksListComponent implements OnInit {

  @Input() tracks: Track[] | null = []

  @Output() trackSelected = new EventEmitter<Track>();

  selectTrack(track: Track) {
    this.trackSelected.emit(track)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
